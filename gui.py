from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from mplwidget import MplWidget

class Form(QWidget):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        nameLabel = QLabel("Name:")
        self.nameLine = QLineEdit()
        self.submitButton = QPushButton("&Submit")

        buttonLayout1 = QVBoxLayout()
        buttonLayout1.addWidget(nameLabel)
        buttonLayout1.addWidget(self.nameLine)
        buttonLayout1.addWidget(self.submitButton)

        self.submitButton.clicked.connect(self.submitContact)

        mainLayout = QGridLayout()
        # mainLayout.addWidget(nameLabel, 0, 0)
        mainLayout.addLayout(buttonLayout1, 0, 1)
        self.mplwidget = MplWidget()
        mainLayout.addWidget(self.mplwidget, 0, 2)

        x = range(0, 10)
        y = range(0, 20, 2)
        self.mplwidget.canvas.ax.plot(x, y, lw=0.5)
        self.mplwidget.canvas.draw()

        self.setLayout(mainLayout)
        self.setWindowTitle("Hello Qt")

    def submitContact(self):
        name = self.nameLine.text()

        if name == "":
            QMessageBox.information(self, "Empty Field",
                                    "Please enter a name and address.")
            return
        else:
            QMessageBox.information(self, "Success!",
                                    "Hello %s!" % name)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    screen = Form()
    screen.show()

    sys.exit(app.exec_())

# import random
# import wave
#
# import sys
#
# import numpy as np
# from PyQt5.QtGui import QWindow
# from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QApplication, QWidget
#
# from main import Ui_MainWindow as Main_UI
#
# from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
# from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
# from matplotlib.figure import Figure
#
#
# class App(QWidget):
#     def __init__(self):
#         super().__init__()
#         self.title = 'PyQt5 simple window - pythonspot.com'
#         self.left = 10
#         self.top = 10
#         self.width = 640
#         self.height = 480
#
#         # a figure instance to plot on
#         self.figure = Figure()
#
#         # this is the Canvas Widget that displays the `figure`
#         # it takes the `figure` instance as a parameter to __init__
#         self.canvas = FigureCanvas(self.figure)
#
#         # this is the Navigation widget
#         # it takes the Canvas widget and a parent
#         #self.toolbar = NavigationToolbar(self.canvas, self)
#
#         # Just some button connected to `plot` method
#         self.button = QPushButton('Plot')
#         #self.button.clicked.connect(self.plot)
#
#         # set the layout
#         layout = QVBoxLayout()
#         #layout.addWidget(self.toolbar)
#         layout.addWidget(self.canvas)
#         layout.addWidget(self.button)
#         self.setLayout(layout)
#         # create an axis
#         ax = self.figure.add_subplot(111)
#
#         # discards the old graph
#         ax.clear()
#
#         f = wave.open("a4_clean.wav", "rb")
#
#         assert f.getnchannels() == 1, "File must be mono"
#         sample_rate = f.getframerate()
#         n_frames = f.getnframes()
#
#         #raw_data = list(f.readframes(n_frames))
#         #raw_data = [random.random() for i in range(10)]
#         signal = f.readframes(-1)
#         signal = np.fromstring(signal, 'Int16')
#         signal = signal[:500]
#         Time = np.linspace(0, len(signal) / sample_rate, num=len(signal))
#         print(raw_data)
#         f.close()
#
#         # plot data
#         ax.plot(Time,signal)
#
#         # refresh canvas
#         self.canvas.draw()
#         self.initUI()
#
#     def initUI(self):
#         self.setWindowTitle(self.title)
#         self.setGeometry(self.left, self.top, self.width, self.height)
#         self.show()
#
#
# if __name__ == '__main__':
#     f = wave.open("a4_clean.wav", "rb")
#
#     assert f.getnchannels() == 1, "File must be mono"
#     sample_rate = f.getframerate()
#     n_frames = f.getnframes()
#
#     raw_data = list(f.readframes(n_frames))
#
#     print(raw_data)
#     f.close()
#
#     app = QApplication(sys.argv)
#     ex = App()
#     sys.exit(app.exec_())
#
#
# class Window(Main_UI):
#     def __init__(self):
#         Main_UI.__init__()
#
#         self.setupUi(self)
#         """# a figure instance to plot on
#         self.figure = Figure()
#
#
#         # this is the Canvas Widget that displays the `figure`
#         # it takes the `figure` instance as a parameter to __init__
#         self.canvas = FigureCanvas(self.figure)
#
#         # this is the Navigation widget
#         # it takes the Canvas widget and a parent
#         self.toolbar = NavigationToolbar(self.canvas, self)
#
#         # Just some button connected to `plot` method
#         self.button = QPushButton('Plot')
#         self.button.clicked.connect(self.plot)
#
#         # set the layout
#         layout = QVBoxLayout()
#         layout.addWidget(self.toolbar)
#         layout.addWidget(self.canvas)
#         layout.addWidget(self.button)
#         self.setLayout(layout)"""
#
#     def plot(self):
#         # create an axis
#         ax = self.figure.add_subplot(111)
#
#         # discards the old graph
#         ax.clear()
#
#         # plot data
#         ax.plot(raw_data, '*-')
#
#         # refresh canvas
#         self.canvas.draw()