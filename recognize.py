import wave
import matplotlib.pyplot as plt
import numpy as np

f = wave.open("a4_violin_1_cropped.wav", "rb")

assert f.getnchannels() == 1, "File must be mono"
sample_rate = f.getframerate()
n_frames = f.getnframes()
print(sample_rate)
raw_data = f.readframes(n_frames)
raw_data = np.fromstring(raw_data, 'Int16')
raw_data = list(raw_data)
raw_data = raw_data[5000:10000]

# since our sample is likely not to start or end at zero, lets cut off both ends to make it so
# that we have a complete number of waves
i = 1
while raw_data[i-1] > 0 and raw_data[i] >= 0:
    i += 1
j = len(raw_data) - 2
while raw_data[j+1] < 0 and raw_data[j] <= 0:
    j -= 1
    print(j)
raw_data = raw_data[i:j]
raw_data.insert(0, -1)
raw_data.append(-1)
zeros = [[], []]
ALLOWED_SPACE = 10
# keep track of how many samples since previous zero, so we don't count too many
# we set it to allowed space because we want to count first crossing
space = ALLOWED_SPACE
for i in range(1, len(raw_data)):
    space += 1
    if (space > ALLOWED_SPACE) \
            and ((raw_data[i - 1] > 0 and raw_data[i] < 0) or (raw_data[i - 1] < 0 and raw_data[i] > 0)):
        space = 0
        zeros[0].append(i)
        zeros[1].append(1)

print(raw_data)
print(sample_rate / len(zeros[0]))
# print((n_frames / sample_rate) / len(zeros[0]))
plt.figure(1)
plt.title('Signal Wave...')
plt.plot(raw_data, lw=0.5)
plt.plot([0 for x in range(len(raw_data))], lw=0.5)
plt.plot(*zeros, "*", lw=0.5)
plt.show()
